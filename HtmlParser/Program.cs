﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using HtmlAgilityPack;
using HtmlParser.classes;
using IniParser;
using IniParser.Model;
using IniParser.Parser;

namespace HtmlParser
{
    class Program
    {

        public class Htmlparser
        {

            public class htmlinfo
            {
                public List<Mod> mods
                {
                    get;
                    set;
                }
                public string header
                {
                    get;
                    set;
                }
            }
            HtmlDocument doctoParse = new HtmlDocument();
            public htmlinfo GetMods()
            {
                var htmlinfo = new htmlinfo();
                htmlinfo.mods = new List<Mod>();
                doctoParse.Load($@"..\ModList.html");
                var nodes = doctoParse.DocumentNode.SelectNodes("//tr");
                htmlinfo.header = doctoParse.DocumentNode.SelectSingleNode("//body/h1").InnerText;

                foreach (var node in nodes)
                {
                    Mod mod = new Mod();
                    var tdnodes = node.SelectNodes("td");
                    for (int i = 0; i < tdnodes.ToArray().Length; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                string modName = tdnodes[i].InnerText;
                                mod.Name = modName;
                                modName.OrderBy(q => q).ToList();
                                break;
                            case 2:
                                string workshopid = tdnodes[i].SelectSingleNode("a").GetAttributeValue("href", "");
                                workshopid = workshopid.Split('=').Last();
                                mod.Workshopid = workshopid;
                                break;
                        }
                    }
                    htmlinfo.mods.Add(mod);

                }
                return htmlinfo;
            }

        }

        class ReadConf
        {
            public List<string> keys
            {
                get;
                set;
            }
            public List<string> keynames
            {
                get;
                set;
            }

            public void readini()
            {
                keys = new List<string>();
                keynames = new List<string>();

                var ConfigFile = new IniDataParser();
                var parser = new FileIniDataParser();
                IniData configParser = parser.ReadFile(@"..\config.ini");
                foreach (SectionData section in configParser.Sections)
                {
                    if (section.SectionName == "File Paths" || section.SectionName == "Optional mods")
                        continue;

                    foreach (KeyData key in section.Keys)
                    {
                        keynames.Add('@' + section.SectionName);
                        keynames.Add(key.Value);
                        keys.Add(key.Value);

                    }


                }
            }

        }


        static void Main(string[] args)
        {

            Htmlparser parse = new Htmlparser();
            var mods = parse.GetMods();
            ReadConf conf = new ReadConf();
            conf.readini();
            var keys = conf.keys;
            var keynames = conf.keynames;
            List<string> nameths = new List<string>();


            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"..\ServerMods.txt"))
            {
                file.WriteLine(mods.header);
                foreach (Mod mod in mods.mods)
                {
                    if (!conf.keys.Contains(mod.Workshopid))
                    {
                        file.WriteLine($"\n[{mod.Name}]");
                        file.WriteLine("Mod ID = " + mod.Workshopid);
                    }
                    if (conf.keys.Contains(mod.Workshopid))
                    {
                        var keyindex = keynames.IndexOf(mod.Workshopid);
                        var nameindex = keyindex - 1;
                        nameths.Add(keynames[nameindex]);
                        
                    }

                }
                file.WriteLine("\nServerMods = " + string.Join(';', nameths));
                Console.WriteLine("ServerMods = M" + string.Join(';', nameths));
            }


        }
    }
}

